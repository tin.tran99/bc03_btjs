/**
 * Input: Nhập chiều dài và chiều rộng của HCN
 * - Gán chiều dài: x = 5 ;
 * - Gán chiều rộng: y = 10 ;
 * -Tính Chu vi: = (x+y)*2
 * -Tính Diện tích = x * y
 * Output: Kết quả Chu vi và Diên tích
 */

var x = 5;
var y = 10;
chuVi= (x+y)*2;
dienTich= x*y;
console.log(chuVi,dienTich);